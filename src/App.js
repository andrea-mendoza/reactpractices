import React from 'react';
import './App.scss';
import RegisterForm from './Views/RegisterForm/RegisterForm';
import UserList from './Views/UserList/UserList';

function App() {
  return (
    <div>
      {/* <h1>Welcome to this work</h1> */}
      {/* <RegisterForm></RegisterForm> */}
      <UserList></UserList>
    </div>
  );
}

export default App;
