// import React from 'react';
// import ReactDOM from 'react-dom';
// import App from './App';


// it('renders without crashing', () => {
//   const div = document.createElement('div');
//   ReactDOM.render(<App />, div);
//   ReactDOM.unmountComponentAtNode(div);
// });


import React from 'react';
import { shallow } from 'enzyme';
import App from './App';
import RegisterForm from './Views/RegisterForm/RegisterForm'
describe('App component', () => {
  it('starts with a count of 0', () => {
    const wrapper = shallow(<App />);
  
    expect(wrapper.find(RegisterForm).length).toEqual(0);
  });
});