import React from 'react';
import { shallow } from 'enzyme';
import RegisterForm from './RegisterForm'


describe('Register Form component', () => {
  it('render without crashing', () => {
    const wrapper = shallow(<RegisterForm />);

    expect(wrapper.exists()).toBe(true);
  });

  it('render the title', () => {
    const wrapper = shallow(<RegisterForm />);
    expect(wrapper.find('h1.form__title').length).toEqual(1);
  });

  it('render the labels', () => {
    const wrapper = shallow(<RegisterForm />);
    expect(wrapper.find('label.label').length).toEqual(4);
  });

  it('render the confirm button', () => {
    const wrapper = shallow(<RegisterForm />);
    
    expect(wrapper.find('button.button--confirm').length).toEqual(1);
  });

  it('handle', () => {
    const wrapper = shallow(<RegisterForm />);
    const nameInput = wrapper.find("input").at(0);
    nameInput.simulate("change", {
      target: { name: "name", value: "Andrea" }
    });
    wrapper.update();
   
    expect(wrapper.state().name).toEqual("Andrea");

  });

  it('when the form is submitted the event  is cancelled',()=>{
    const wrapper = shallow(<RegisterForm />);
    let prevented = false;
    wrapper.find("form").simulate("submit", {
      preventDefault: () => {
        prevented = true;
      }
    });
  });

  
});