import React, { Component } from 'react'
import './RegisterForm.scss'
import '../../CommonBlocks/Button.scss'
import '../../CommonBlocks/Label.scss'
import '../../CommonBlocks/Input.scss'

export default class RegisterForm extends Component {

  constructor(){
    super();
    this.state = {
      name: "",
      lastname: "",
      email: "",
      password: ""
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  render() {
    return(
      <div className="form">
        <div className="form__container">
          <h1 className="form__title">Registro de Personal</h1>
          <form className="form__grid" onSubmit={this.handleSubmit}>
            <label className="label">Nombre  :</label>
            <input type="text" className="input" name="name" value={this.state.name} onChange={this.handleChange} />
            
            <label className="label">Apellidos :</label>
            <input className="input" type="text" name="lastname" value={this.state.lastname} onChange={this.handleChange}/>

            <label className="label">Correo Electronico:</label>
            <input className="input" type="email" name="email" onChange={this.handleChange}/>

            <label className="label">Contraseña:</label>
            <input className="input" type="password" name="password" onChange={this.handleChange}/>
            
            <button type="submit" className="button button--confirm">Confirmar</button>
            <button  className="button button--cancel">Cancelar</button>
          </form>
        </div>
      </div>
    )
  }
}