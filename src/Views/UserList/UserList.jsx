import React, { Component } from 'react'
import './UserList.scss'

export default class UserList extends Component {

  constructor(props){
    super(props);
    this.fields = [];
    this.users = [];
    this.state = {
      users:[]
    }

    this.compareBy.bind(this);
    this.sortBy.bind(this);
  }

  compareBy(key) {
    return function (a, b) {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
      return 0;
    };
  }
 
  sortBy(key) {
    let arrayCopy = [...this.state.users];
    arrayCopy.sort(this.compareBy(key));
    this.setState({users: arrayCopy});
  }

  componentDidMount(){
    fetch('http://jsonplaceholder.typicode.com/todos')
    .then(res => res.json())
    .then((data) => {
      this.setState({ users: data })
      this.users = data;
    },
      (error) => {
        this.setState({
          error
        });
      }
    )
  }

  filterList(event,key){
    let searchValue = event.target.value.toLowerCase();
    let updatedList = this.users;
    if(searchValue === ""){
      this.setState({users: this.users});
    }
    else{
      updatedList = updatedList.filter(
        function (str) { 
          return (str[key]).toString().toLowerCase().indexOf(searchValue) !== -1; 
        }
      );
      this.setState({users: updatedList});
    }
  }

  editUser(user){
    alert("Fuciona para " + user);
  }

  deleteUser(userId){
    alert("Funciona borrar para "+ userId)
  }

  renderTable(){
    return this.state.users.map(
      user => {
        return (
          <tr key={user.id}>
            <td>{user.id}</td>
            <td>{user.title}</td>
            <td>
              <button className="material-icons button" onClick={() => this.editUser(user.id)}>edit</button>
              <button className="material-icons button" onClick={() => this.deleteUser(user.id)}> delete</button>
            </td>
          </tr>
        )
      }
    )
  }

  render() {
    return(
      <div>
        <div>
          <form>
            <fieldset >
            <input type="text" placeholder="Search"  onChange={e => {this.filterList(e,'title')}}/>
            </fieldset>
          </form>
        </div>
        <table>
          <tbody>
              <tr>
                <th onClick={() => this.sortBy('id')} >Id </th>
                <th onClick={() => this.sortBy('title')}> Titulo </th>
              </tr>
            {this.renderTable()}
          </tbody>
        </table>
      </div>
      
    )
  }
}


// console.clear();


//     class TodoApp extends React.Component {
//       constructor() {
//         super();
//         this.state = {
//           todos: ['a','b','c','d','e','f','g','h','i','j','k'],
//           currentPage: 1,
//           todosPerPage: 4
//         };
//         this.handleClick = this.handleClick.bind(this);
//       }

//       handleClick(event) {
//         this.setState({
//           currentPage: Number(event.target.id)
//         });
//       }
      
//       changeValue(event){
        
//         this.setState({
//           todosPerPage: Number(event.target.value)
//         });
        
//         alert("Se quiere cambiar el valor " + event.target.value);
//       }

//       render() {
//         const { todos, currentPage, todosPerPage } = this.state;

//         // Logic for displaying current todos
//         const indexOfLastTodo = currentPage * todosPerPage;
//         const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
//         const currentTodos = todos.slice(indexOfFirstTodo, indexOfLastTodo);

//         const renderTodos = currentTodos.map((todo, index) => {
//           return <li key={index}>{todo}</li>;
//         });

//         // Logic for displaying page numbers
//         const pageNumbers = [];
//         for (let i = 1; i <= Math.ceil(todos.length / todosPerPage); i++) {
//           pageNumbers.push(i);
//         }

//         const renderPageNumbers = pageNumbers.map(number => {
//           return (
//             <li
//               key={number}
//               id={number}
//               onClick={this.handleClick}
//             >
//               {number}
//             </li>
//           );
//         });

//         return (
//           <div>
//             <ul>
//               {renderTodos}
//             </ul>
//             <ul id="page-numbers">
//               {renderPageNumbers}
//             </ul>
//             <ul>
//               <select onChange={e => {this.changeValue(e)}} value={this.state.value}>
//                 <option value="3">3</option>
//                 <option value="4">4</option>
//                 <option value="5">5</option>
//               </select>
//             </ul>
//           </div>
//         );
//       }
//     }


//     ReactDOM.render(
//       <TodoApp />,
//       document.getElementById('app')
//     );